import Vue from "vue";

import BaseCard from "@/components/UL/BaseCard";
import BaseButton from "@/components/UL/BaseButton";

Vue.component("BaseButton", BaseButton);
Vue.component("BaseCard", BaseCard);
