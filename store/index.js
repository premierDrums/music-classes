import Vuex from "vuex";
const createStore = () => {
  return new Vuex.Store({
    namespaced: true,
    state: {
      posts: [
        {
          id: "1",
          title: "First Post",
          previewText: "This is our first post!",
          thumbnail:
            "https://static01.nyt.com/images/2020/04/15/business/15Techfix-illo/15Techfix-illo-mediumSquareAt3X.jpg"
        },
        {
          id: "2",
          title: "Second Post",
          previewText: "This is our second post!",
          thumbnail:
            "https://static01.nyt.com/images/2020/04/15/business/15Techfix-illo/15Techfix-illo-mediumSquareAt3X.jpg"
        }
      ],

      coaches: [
        {
          id: "c3",
          firstName: "Full Name:",
          image:
            "https://images.pexels.com/photos/280221/pexels-photo-280221.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
          lastName: "",
          areas: ["lesson", "classes", "band "],
          description: "Description: ",
          Hourlyrate: ""
        }
      ]
    },

    mutations: {
      //formCoach
      registerCoach(state, payload) {
        state.coaches.push(payload); //push as a new coach,payl is formdata
      },
      setPosts(state, posts) {
        state.loadedPosts = posts;
      }, //payload post
      setPosts(state, posts) {
        state.LoadedPost = posts;
      }
    },
    actions: {
      async registerCoach(context, data) {
        //data is payload
        const userId = context.rootGetters.userId;
        const coachData = {
          firstName: data.first,
          lastName: data.last,
          description: data.desc,
          hourlyrate: data.hourlyrate,
          areas: data.areas
        };
        const response = await fetch(
          `https://music-classes-c6830-default-rtdb.firebaseio.com/coach/${userId}.json`,
          {
            //second argument object
            method: "PUT",
            body: JSON.stringify(coachData)
          }
        );

        // const responseData = await response.json();
        if (!response.ok) {
          //error
        }

        context.commit("registerCoach", {
          ...coachData,
          id: userId
        }); //as a new object
      }
    },
    getters: {
      coaches(state) {
        return state.coaches;
      },
      hasCoaches(state) {
        return state.coaches && state.coaches.length > 0;
      },
      //// coaches Reviews","
      posts(state) {
        return state.posts;
      }
    }
  });
};

export default createStore;
