export default function({ route, redirect }) {
  if (route.path !== "/singnin") {
    if (!$fire.auth.currentUser) {
      return redirect("/singnin");
    }
  } else if (route.path === "/singnin") {
    if (!$fire.auth.currentUser) {
    } else {
      return redirect("/");
    }
  }
}
